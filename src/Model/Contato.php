<?php

namespace Api\Model;

class Contato
{
    public $email;
    public $telefone;

    public function __construct($email, $telefone)
    {
        $this->email = $email;
        $this->telefone = $telefone;
    }
}
