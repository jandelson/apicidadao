<?php

namespace Api\Model;

class Cidadao
{
    public $nome;
    public $sobrenome;
    public $cpf;

    public function __construct($nome, $sobrenome, $cpf)
    {
        $this->nome = $nome;
        $this->sobrenome = $sobrenome;
        $this->cpf = $cpf;
    }
}
