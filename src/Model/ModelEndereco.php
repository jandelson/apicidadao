<?php

namespace Api\Model;

class ModelEndereco
{
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function insertEndereco(Endereco $endereco, string $cidadao_id): string
    {
        $sql = $this->pdo->prepare('
            insert into endereco 
                (logradouro,cep,cidade,bairro,uf,cidadao_id) values 
                (:logradouro,:cep,:cidade,:bairro,:uf,:cidadao_id)
        ');
        $sql->execute([
            ':logradouro' => $endereco->logradouro,
            ':cep' => $endereco->cep,
            ':cidade' => $endereco->cidade,
            ':bairro' => $endereco->bairro,
            ':uf' => $endereco->uf,
            ':cidadao_id' => $cidadao_id
        ]);
        
        return $this->pdo->lastInsertId();
    }

    public function updateEndereco(Endereco $endereco, $cidadao_id): int
    {
        $sql = $this->pdo->prepare('
            update endereco set 
                logradouro = :logradouro,
                cep = :cep,
                cidade = :cidade,
                bairro = :bairro,
                uf = :uf
            where cidadao_id = :cidadao_id
        ');
        $sql->execute([
            ':logradouro' => $endereco->logradouro,
            ':cep' => $endereco->cep,
            ':cidade' => $endereco->cidade,
            ':bairro' => $endereco->bairro,
            ':uf' => $endereco->uf,
            ':cidadao_id' => $cidadao_id
        ]);

        return $sql->rowCount();
    }

    public function delete(strig $cidadao_id): int
    {
        $sql = $this->pdo->prepare('
            delete from endereco where cidadao_id = :cidadao_id
        ');
        $sql->execute([
            ':cidadao_id' => $cidadao_id
        ]);

        return $sql->rowCount();
    }
}
