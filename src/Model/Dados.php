<?php

namespace Api\Model;

use Api\Model\ModelCidadao;
use Api\Model\ModelEndereco;
use Api\Model\ModelContato;
use Api\DataBase\Sqlite;

class Dados
{
    public $pdo;

    public function __construct()
    {
        $this->pdo = new Sqlite();
    }

    public function findAll($cpf = 0): array
    {
        $where = 'where cpf > :cpf';
        if ($cpf != 0) {
            $where = 'where cpf = :cpf';
        }
        $sql = $this->pdo->prepare('
            SELECT distinct
                cidadao.nome,
                cidadao.cpf,
                endereco.logradouro,
                endereco.cidade,
                endereco.bairro,
                endereco.uf,
                endereco.cep,
                contato.email,
                contato.telefone
            FROM 
                cidadao
                left join endereco on endereco.cidadao_id = cidadao.id
                left join contato on contato.cidadao_id = cidadao.id
                ' . $where . '
        ');

        $sql->execute(
            [':cpf' => $cpf]
        );
        $data = $sql->fetchAll($this->pdo::FETCH_OBJ);
        return $data;
    }

    public function create(
        Cidadao $cidadao,
        Endereco $endereco,
        Contato $contato
    ): array {
        $dados = $this->findAll($cidadao->cpf);
        if ($dados[0]->cpf > 0) {
            return [
                'msg' => 'Cidadão já Cadastrado!',
                'status' => 202
            ];
        }

        $id = (new ModelCidadao($this->pdo))->insertCidadao($cidadao);

        (new ModelEndereco($this->pdo))->insertEndereco($endereco, $id);

        (new ModelContato($this->pdo))->insertContato($contato, $id);

        return [
            'msg' => 'Cidadão cadastrado - cpf: ' . $cidadao->cpf,
            'status' => 201
        ];
    }

    public function update(
        Cidadao $cidadao, 
        Endereco $endereco, 
        Contato $contato, 
        $cidadao_id
    ): array {
        (new ModelCidadao($this->pdo))->updateCidadao($cidadao, $cidadao_id);

        (new ModelEndereco($this->pdo))->updateEndereco($endereco, $cidadao_id);

        (new ModelContato($this->pdo))->updateContato($contato, $cidadao_id);

        return [
            'msg' => 'Cidadão atualizado! - cpf: ' . $cidadao->cpf,
            'status' => 201
        ];
    }

    public function delete(string $cpf)
    {
        $sql = $this->pdo->prepare('
            delete from cidadao where cpf = :cpf
        ');
        $sql->bindParam(':cpf', $cpf);
        $sql->execute();
        
        return [
            'msg' => 'Cidadão Excluido! - cpf: ' . $cpf,
            'status' => 201
        ];
    }
}
