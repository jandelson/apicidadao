<?php

namespace Api\Model;

class Endereco
{
    public $cep;
    public $logradouro;
    public $bairro;
    public $cidade;
    public $uf;

    public function __construct($cep, $logradouro, $bairro, $cidade, $uf)
    {
        $this->cep = $cep;
        $this->logradouro = $logradouro;
        $this->bairro = $bairro;
        $this->cidade = $cidade;
        $this->uf = $uf;
    }
}
