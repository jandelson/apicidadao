<?php

namespace Api\Model;

class ModelCidadao
{
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    /**
     * Insert Cidadao
     */
    public function insertCidadao($cidadao): string
    {
        $sql = $this->pdo->prepare('
            insert into cidadao 
                (nome,sobrenome,cpf) values 
                (:nome,:sobrenome,:cpf)
        ');
        $sql->execute([
            ':nome' => $cidadao->nome,
            ':sobrenome' => $cidadao->sobrenome,
            ':cpf' => $cidadao->cpf
        ]);

        return $this->pdo->lastInsertId();
    }

    public function updateCidadao($cidadao, $cidadao_id): int
    {
        $sql = $this->pdo->prepare('
            update cidadao set 
                nome = :nome,
                sobrenome = :sobrenome,
                cpf = :cpf
            where id = :id
        ');
        $sql->execute([
            ':nome' => $cidadao->nome,
            ':sobrenome' => $cidadao->sobrenome,
            ':cpf' => $cidadao->cpf,
            ':id' => $cidadao_id
        ]);
        
        return $sql->rowCount();
    }
    
    public function delete($cidadao_id): int
    {
        $sql = $this->pdo->prepare('
            delete from cidadao where cidadao_id = :cidadao_id
        ');
        $sql->execute([
            ':cidadao_id' => $cidadao_id
        ]);

        return $sql->rowCount();
    }
}
