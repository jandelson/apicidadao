<?php

namespace Api\Model;

class ModelContato
{
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    /**
     * Insert Contato
     */
    public function insertContato(Contato $contato, string $cidadao_id): string
    {
        $sql = $this->pdo->prepare('
            insert into contato 
                (email,telefone,cidadao_id) values 
                (:email,:telefone,:cidadao_id)
        ');
        $sql->execute([
            ':email' => $contato->email,
            ':telefone' => $contato->telefone,
            ':cidadao_id' => $cidadao_id
        ]);

        return $this->pdo->lastInsertId();
    }

    public function updateContato(Contato $contato, string $cidadao_id): int
    {
        $sql = $this->pdo->prepare('
            update contato 
            set email = :email,telefone = :telefone
            where cidadao_id = :cidadao_id
        ');

        $sql->execute([
            ':email' => $contato->email,
            ':telefone' => $contato->telefone,
            ':cidadao_id' => $cidadao_id
        ]);

        return $sql->rowCount();
    }

    public function deleteContato(strig $cidadao_id): int
    {
        $sql = $this->pdo->prepare('
            delete from contato where cidadao_id = :cidadao_id
        ');
        $sql->execute([
            ':cidadao_id' => $cidadao_id
        ]);

        return $sql->rowCount();
    }
}
