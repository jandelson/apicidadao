<?php

namespace Api\Service;

use Api\Model\Endereco;

class ViaCep
{
    public function getcep($cep): Endereco
    {
        $url = "https://viacep.com.br/ws/{$cep}/json/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        
        return new Endereco(
            $cep,
            $result->logradouro,
            $result->bairro,
            $result->localidade,
            $result->uf
        );
    }
}
