<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;
use Slim\Container;
use Api\Model\Dados;
use Api\Model\Cidadao;
use Api\Model\Contato;
use Api\Service\ViaCep;
use Api\Docs\Swagger;

require './vendor/autoload.php';

$app = AppFactory::create();

$app->get('/', Swagger::class);

$app->group('/cidadao', function (RouteCollectorProxy $group) use ($app) {
    /**
     * Listar todos
     */
    $group->get(
        '/listar',
        function (Request $request, Response $response) {
            $dados = (new Dados())->findAll();
            $response->getBody()->write(json_encode($dados));
            return $response->withHeader('Content-Type', 'application/json');
        }
    );
    /**
     * Consulta por CPF
     */
    $group->get(
        '/listar/{cpf}',
        function (Request $request, Response $response, array $params) {
            $cpf = $params['cpf'];
            $dados = (new Dados())->findAll($cpf);
            $response->getBody()->write(json_encode($dados));
            return $response->withHeader('Content-Type', 'application/json');
        }
    );
    /**
     * Cadastrar cidadao
     */
    $group->post(
        '/cadastrar',
        function (Request $request, Response $response) {
            $params = (object) $request->getQueryParams();

            $dados = (new Dados())->create(
                new Cidadao($params->nome, $params->sobrenome, $params->cpf),
                (new ViaCep())->getcep($params->cep),
                new Contato($params->email, $params->telefone)
            );

            $response->getBody()->write(json_encode(['msg' => $dados['msg']]));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($dados['status']);
        }
    );
    /**
     * Atualiza cidadao
     */
    $group->put(
        '/atualizar/{cpf}',
        function (Request $request, Response $response, array $params) {
            $cpf = $params['cpf'];
            $dados = (new Dados())->findAll($cpf);
            $params = (object) $request->getQueryParams();

            $dados = (new Dados())->update(
                new Cidadao($params->nome, $params->sobrenome, $cpf),
                (new ViaCep())->getcep($params->cep),
                new Contato($params->email, $params->telefone),
                $dados->id
            );

            $response->getBody()->write(json_encode(['msg' => $dados['msg']]));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($dados['status']);
        }
    );
    /**
     * Deletar cidadao
     */
    $group->delete(
        '/deletar/{cpf}',
        function (Request $request, Response $response, array $params) {
            $cpf = $params['cpf'];
            $dados = (new Dados())->delete($cpf);

            $response->getBody()->write(json_encode(['msg' => $dados['msg']]));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($dados['status']);
        }
    );
});


$app->run();
